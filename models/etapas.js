var stages = {
    0: {
        descripcion: "Bienvenido",
        obj: require("../etapas/0"),
    },
    1: {
        descripcion: "Nivel 1",
        obj: require("../etapas/1"),
    },
    2: {
        descripcion: "Nivel 2",
        obj: require("../etapas/2"),
    },
    3: {
        descripcion: "Nivel 3",
        obj: require("../etapas/3"),
    },
    4: {
        descripcion: "Nivel 4",
        obj: require("../etapas/4"),
    },
    5: {
        descripcion: "Nivel 5",
        obj: require("../etapas/5"),
    },
};

exports.step = stages;