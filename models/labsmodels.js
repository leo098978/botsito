const mongoose = require("mongoose");



const LaboratorioSchema = new  mongoose.Schema (
    {   
        ID:{ type: String},
        Nombre: { type: String},
        Capacidad:{ type: String}

    }
)

module.exports = mongoose.model("Laboratorio", LaboratorioSchema);
