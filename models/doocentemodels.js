const mongoose = require("mongoose");



const DocenteSchema = new  mongoose.Schema (
    {   
        ID:{ type: String},
        Nombre: { type: String},
        Asignatura:{ type: String}

    }
)

module.exports = mongoose.model("Docente", DocenteSchema);
