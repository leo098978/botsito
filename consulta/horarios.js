const Horario = {
    1: {
      ID: "1" ,
      HoraInicio: "08:00 AM",
      HoraFin: "10:00 AM"
    },
    2: {
        ID: "2" ,
        HoraInicio: "11:00 AM",
        HoraFin: "13:00 PM"   
    },
    3: {
        ID: "3" ,
      HoraInicio: "14:00 PM",
      HoraFin: "16:00 PM"   
    },
    4: {
        ID: "4" ,
      HoraInicio: "17:00 PM",
      HoraFin: "19:00 PM"   
    },
    
  };
  
  exports.horas = Horario;