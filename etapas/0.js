
const { menu0 } = require("../menu/menu0");
const { db } = require("../models/banco");

function execute(user, msg, contato) {

    // Obtenga la hora actual de la PC para establecer si será Buenos días, tarde o noche.
    stamp = new Date();
    

    hours = stamp.getHours();
    if (hours >= 18 && hours < 24) {
        time = "Buenas noches"
    } else if (hours >= 12 && hours < 18) {
        time = "Buenas tardes"
    } else if (hours >= 0 && hours < 12) {
        time = "Buentos dias"
    }
    
    let menu = " MENU DE OPCIONES\n\n";

    Object.keys(menu0).forEach((value) => {
        let element = menu0[value];
        menu += `${value} - ${element.description} \n`;
    });

    db[user].stage = 1;

    return [
        menu,
        "Soy asistente virtual, te presentaré el menu de opciones"
       
    ];
}

exports.execute = execute;